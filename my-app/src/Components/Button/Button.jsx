import React from 'react';

// import styles from '../Button/Button.module.scss'
// import styles from '../Modal/Modal.module.scss'

class Button extends React.Component{
   

    render(){
                 return  (<button className={this.props.className} onClick={()=>{this.props.onClick(this.props.name,this.props.id)} } > {this.props.title} </button>)
    } 
}

export default Button;