import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button';
// import Buttonok from '../Buttonok/Buttonok';


class Modal extends React.Component {


    
    
    
    render(){
        
        const {closeModal, closemodalok, addToBasket, name, id} = this.props;
        
        return( 
        <div className={styles.wrapperRed} onClick={closeModal} >
                <div className={styles.modal} onClick={e=>{
                 e.preventDefault();
                 e.stopPropagation();
                 }
                 }>
                <div className={styles.header}>
                    <h1 className={styles.title}>Отличный Выбор!</h1>
                    <Button className={styles.buttonEsc} onClick={closeModal} title={String.fromCharCode(215)}/>
                </div>
                <div className={styles.content}> Положить в корзину? </div>
                <div className={styles.buttons}>
                    <Button className={styles.buttonCancel} addToBasket={addToBasket} onClick={()=>{closemodalok(name,id)} } title="Ok"/>
                    <Button className={styles.buttonCancel} onClick={closeModal} title='Отмена'/>
                    </div>
                </div>
            </div>)
        }
    }

    export default Modal;