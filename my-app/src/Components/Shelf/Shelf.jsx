import React, {Component} from "react";
import Modal from "../Modal/Modal";
import Card from "../Card/Card";
import styles from "./Shelf.module.scss";



class Shelf extends Component{
    constructor(props){
        super(props);
        this.state ={
            isOpened1: false,
            
        }
    };
  
    openModal=(name,id)=>{
        
        
        return (
            
        this.setState(curent => ({...curent, isOpened1 : !curent.isOpened1, name:name,id:id})),
        document.body.style.overflow = "hidden",
        <Modal name= {name} id = {id}/>
        
        )

        
       
      }
    closemodalok=(name,id,state)=>{

        return (this.setState(curent => ({...curent, isOpened1 : !curent.isOpened1})),
        document.body.style.overflow = "",
        this.addToLocalestorage(this.state.name,this.state.id)
                       
        )
        
    }
    
    closeModal=()=>{
       
        return (this.setState(curent => ({...curent, isOpened1 : !curent.isOpened1})),
        document.body.style.overflow = ""
        
        )
    }

    addToLocalestorage = (name,id) => {

        console.log('addToLocalestorage работает',name,id);
        
        // localStorage.getItem ('books')
        
            localStorage.setItem(
                id, JSON.stringify( [{
                    name:name,
                }])
            )

       
        
    }
   
    render(){
        
        <Modal 
        // books= 
        // {this.props}
         openModal={this.openModal} closemodalok={ ()=>{this.closemodalok(this.state.name, this.state.id)}} closeModal={this.closeModal} />
        const {books}= this.props

        let booksOnShelf = books.map(({name ,price, color, url, art, id, isFavorite})=>{return (<Card key = {id} name={name} price={price} color={color} url={url} art={art} id={id} addToLocalestorage={this.addToLocalestorage} isFavorite={isFavorite} openModal={this.openModal} />)})
        

       


        return (
            <>{this.state.isOpened1 && <Modal book={this.props} closemodalok={this.closemodalok}closeModal={this.closeModal}/>
            }
            <div className={styles.shelf}>{booksOnShelf}</div></>
        )

}}


export default Shelf