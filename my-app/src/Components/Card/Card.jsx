import React, {Component} from "react";
import styles from './Card.module.scss';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';




class Card extends Component {
    constructor(props){
        super(props);
        this.state={
            name: this.props.name,
            id: this.props.id,
            isFavorite:false,
            
        }
    }

    favorite = (state) => {
        console.log('в фаворит',this.state.name,this.state.id);
        // this.props.addToLocalestorage(this.state.name,this.state.id);

        if (this.state.isFavorite){
            
            this.setState(curent=>({...curent,isFavorite :false}))}
        else {
            this.props.addToLocalestorage(this.state.name,this.state.id);
            this.setState(curent=>({...curent,isFavorite :true
            }))
            
        }
        return
   }


    render(){
    const{openModal,name,price,color,art,id}=this.props;
    <Modal name={name} id={id}/>
    
          return (          
      <div className={styles.book}> 
      <img className={styles.img} src={this.props.url} alt='img'></img>
      {
      this.state.isFavorite===true?
        <i className="far fa-bell"  style={{color: 'green'}} onClick={()=>{this.favorite(this.state.isFavorite)} }></i>:
        <i className="far fa-bell"  style={{color: 'red'}} onClick={()=>{this.favorite(this.state.isFavorite)} }></i>
      }
      
      <p>{name}</p>
      <p>{price}</p>
      <p>{art}</p>
      <p>{color}</p>
      <Button className={styles.btn} onClick={()=>{openModal(this.state.name,this.state.id)}} title="В корзину" name={this.state.name} id={this.state.id} /> 
      </div>
      )
    }
}

export default Card