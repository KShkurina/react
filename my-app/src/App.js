import React, {Component} from 'react';
import styles from'./App.module.scss';
import Shelf from './Components/Shelf/Shelf';
// import Basket from './Components/Basket/Basket';
// import Modal from './Components/Modal/Modal';

class App extends Component{
  constructor(props){
    super(props);

    this.state = {
      books : []
    }

  }
  

  async componentDidMount(){
    const response = await fetch('./data.json').then(response => response.json())
    this.setState(
      {books : response}
    )
        
  }
  
  

render(){
  const {books} = this.state
  
  return (
    <div className={styles.App}>
      {/* <Basket books={books}/>     */}
      <Shelf books= {books}/>
    </div>
  )
} 
 
}

export default App;