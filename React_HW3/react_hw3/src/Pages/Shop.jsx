import React from "react";
import Card from "../Components/Card/Card";
import Modal from "../Components/Modal/Modal";
import styles from "./Shop.module.scss"


const Shop = (props) => {

    const { closeModal, openModal, modalConfirm, openedModal, favorites, setFavorites } = props

    const books = props.data.map(
        book => {
            return (
                <Card data={book} favorites={favorites} setFavorites={setFavorites} openModal={openModal} />
            )
        }
    )
    
    return (
        <div className={styles.shop}>
            <Modal openedModal={openedModal}
                closeModal={closeModal}
                modalConfirm={modalConfirm}
                openModal={openModal} />
            {books}
        </div>
    )

}
export default Shop