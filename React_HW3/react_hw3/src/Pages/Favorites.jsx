import React from "react";
import { useState } from "react";
import styles from "./Favorites.module.scss"

const Favorites = () =>{

    const [favoritBook,setfavoritBook]= useState()
    
    let favorites = localStorage.getItem('favorites')
    
    let favoritesBooks = JSON.parse(favorites)

   if(favoritesBooks.length===0){return <p>Вы еще не выбрали книгу</p>}

    let book = favoritesBooks.map(book=><div className={styles.book}> <p>{book.name}</p><button
     onClick={()=>{favoriteRemover(book.id)}}
     >Отменить выбор</button>
        </div>)

    
 const favoriteRemover=(id)=>{
   let filtredBook = favoritesBooks.find(book=>book.id===id);
   if (filtredBook)
      {
         let filtredBooks = favoritesBooks.filter(book=>book.id!==id)
         localStorage.setItem('favorites', JSON.stringify( filtredBooks))
         setfavoritBook(book)
      }
}

    return <p className={styles.favorites}> {book} </p>
}
export default Favorites