import React, { useState } from "react";
import Card from "../Components/Card/Card";
import CartItem from "../Components/CartItem/CartItem";
import styles from "./Cart.module.scss"

const Cart = ()=>{
    const [book,setBook]=useState()
    

    let items = localStorage.getItem('items');

    if(items === null) 
        return <p>В корзине ничего нет</p>

    const removeBook = (id)=>{
        let books = JSON.parse(items);
        let filteredBooks = books.filter(book=>book.id!==id)
        
        setBook(filteredBooks)
        if(filteredBooks.length === 0){
            localStorage.removeItem('items')

            return <p>В корзине ничего нет</p>
        }
        else 
            localStorage.setItem('items',JSON.stringify(filteredBooks))
    }   
    
    let cartItems= JSON.parse(items).map(element =><CartItem 
        name={element.name}
        id={element.id}
        price={element.price} 
        removeBook={removeBook}
        />)

    return (
        <div className={styles.cart}>{cartItems}
        </div>
       
    )
    
    
}
export default Cart