import React from "react";
import {NavLink} from "react-router-dom";
import style from './Header.component.scss'

const links = {
    'Shop': '/shop',
    'Cart': '/cart',
    'Favorites': '/favorites'
}
const Header = () => {
    const allLinks = Object.entries(links).map(([name, link],key) =>
         <NavLink to={link} key={key} className='link' activeClassName='active_link'>{name}</NavLink>
    )
    return (
        <div>{allLinks}</div>

    )
}
export default Header