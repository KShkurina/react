import React from "react";
import styles from "./CartItem.module.scss"


const CartItem = ({id,name,price,removeBook})=>{

    console.log(id,name,price);
return <div className={styles.cartItem}>
    <p>Название :{name}</p>
    
    <p>Цена: {price}</p>
    <button onClick={()=>{removeBook(id)}}>Remove</button>
    
    {/* <button onClick={removeBook}>Remove</button> */}
    </div>

}
export default CartItem
