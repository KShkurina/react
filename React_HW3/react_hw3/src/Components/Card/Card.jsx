import React from "react";
import styles from './Card.module.scss';
import Button from '../Button/Button';


const Card =(props)=>{    
    const {name,id,price,art,color,url,isFavorite} = props.data
    const {favorites,setFavorites} = props
    console.log(favorites,id);

    let isFavoriteBook = favorites.find(favorite=>favorite.id===id)
    const FavoriteQ= isFavoriteBook?true:isFavorite
    
    const toggleFavorite = (name,id) => {
        let favorites = localStorage.getItem('favorites')

        if (favorites===null){
            
            let favoriteBook = [{id:id,name:name}]
            localStorage.setItem('favorites',JSON.stringify(favoriteBook))
            setFavorites([{id:id,name:name}])
        }
        else{            
            let favoriteBooks = JSON.parse(favorites)
            
            let bookFind = favoriteBooks.find(book=>book.id===id)

            if(bookFind) favoriteBooks = favoriteBooks.filter(book=>book.id!==id)
            else favoriteBooks.push({id:id,name:name})

            localStorage.setItem('favorites',JSON.stringify(favoriteBooks))
            setFavorites(favoriteBooks)
            
            if(favoriteBooks.length===0){localStorage.removeItem('favorites')}
        }
   }
          return (          
      <div 
      className={styles.book}
      > 
      <img className={styles.img} src={url} alt='img'></img>
      <i className="far fa-bell"  style={{color: FavoriteQ===true? 'green':'red'}}
       onClick={()=>{toggleFavorite(name,id)}}></i>
      <p>{name}</p>
      <p>{price}</p>
      <p>{art}</p>
      <p>{color}</p>
      <Button className={styles.btn} onClick={()=>{props.openModal(name,id,price)}} title="В корзину" name={name} id={id} /> 
      </div>
      )
    
}

export default Card