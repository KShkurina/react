import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button';
// import Buttonok from '../Buttonok/Buttonok';


const Modal = (props)=> {
  
        
      
        const {closeModal,modalConfirm,openedModal}= props
        console.log(openedModal.name);
        if(!openedModal.isOpened){return null}
        return( 
        <div className={styles.wrapperRed} onClick={closeModal} >
                <div className={styles.modal} onClick={e=>{
                 e.preventDefault();
                 e.stopPropagation();
                 }
                 }>
                <div className={styles.header}>
                    <h1 className={styles.title}>"{openedModal.name}"
                     - Отличный Выбор!</h1>
                    <Button className={styles.buttonEsc} onClick={closeModal} title={String.fromCharCode(215)}/>
                </div>
                <div className={styles.content}> Сегодня ее цена всего {openedModal.price} грн.  Положить в корзину? </div>
                <div className={styles.buttons}>
                    <Button className={styles.buttonCancel}  onClick={()=>{modalConfirm(openedModal.name,openedModal.id,openedModal.price)} } title="Ok"/>
                    <Button className={styles.buttonCancel} onClick={closeModal} title='Отмена'/>
                    </div>
                </div>
            </div>)
        
    }

    export default Modal;