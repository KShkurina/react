import './App.css';
import  {useEffect , useState} from "react";
import Header from "./Pages/Header";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Shop from './Pages/Shop';
import Favorites from './Pages/Favorites';
import Cart from './Pages/Cart';
import Modal from './Components/Modal/Modal';

function App() {
    const [data, setData] = useState([]);
    const [openedModal, setOpenedModal] = useState({isOpened:false});
    const [favorites, setFavorites] = useState([]);

   
    useEffect(() => {
    
             fetch('./data.json')
            .then(response => response.json())
            .then(data=>setData(data))
        
        let favoriteBook = localStorage.getItem('favorites')
        if (favoriteBook!==null)
        {
            favoriteBook=JSON.parse(favoriteBook)
            setFavorites(favoriteBook)
            console.log('books',favoriteBook);
        }
    },[])

    const openModal=(name,id,price)=>{
        setOpenedModal({isOpened : !openedModal.isOpened, name:name,id:id,price:price});
        document.body.style.overflow = "hidden";
        <Modal  name= {name} id = {id}/>
      }


    const modalConfirm = (name,id,price)=>{

        let items = localStorage.getItem('items');
        if(items !== null) {
            let itemsJson = JSON.parse(items);
            itemsJson.push({id: id,name: name, price:price});
            localStorage.setItem('items', JSON.stringify(itemsJson));
        } else
            localStorage.setItem('items', JSON.stringify([
                {
                    id: id,
                    name: name,
                    price:price
                }
            ]));
            closeModal()
    }
    
    const closeModal=()=>{
       
        setOpenedModal({isOpened : !openedModal.isOpened})
        document.body.style.overflow = ""
    }

    return (
        
        <Router>
            <div className="App">
                <Header/>
                <Switch>
                    <Route path="/shop"  render={() =>
                        <Shop 
                        data={data} 
                        openedModal={openedModal}
                        closeModal={closeModal} 
                        modalConfirm={modalConfirm} 
                        openModal={openModal}
                        favorites={favorites}
                        setFavorites={setFavorites}/>
                    }/>
                    <Route path='/cart' render={()=><Cart/>}/>
                    <Route path='/favorites' render={()=><Favorites/>}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
