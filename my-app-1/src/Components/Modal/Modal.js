import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button';


class Modal extends React.Component {
    render(){
        
        const {closeModal} = this.props
        return( 
        <div className={styles.wrapperRed} onClick={closeModal} >
                <div className={styles.modal} onClick={e=>{console.log(e.target);
                 e.preventDefault();
                 e.stopPropagation()}}>
                <div className={styles.header}>
                    <h1 className={styles.title}>Do you want to delite this file?</h1>
                    <Button className={styles.buttonEsc} closeModal= {closeModal} title={String.fromCharCode(215)}/>
                </div>
                <div className={styles.content}> {this.props.text} <br/> {this.props.textAdd} </div>
                <div className={styles.buttons}>
                    <Button className={styles.buttonOk} title='Ok'/>
                    <Button className={styles.buttonCancel} closeModal= {closeModal} title='Cancel'/>
                    </div>
                </div>
            </div>)
        }
    }

    export default Modal;