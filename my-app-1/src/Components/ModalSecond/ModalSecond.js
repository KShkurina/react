import React from 'react';
import styles from '../ModalSecond/ModalSecons.module.scss';
import Button from '../Button/Button';


class ModalSecond extends React.Component {
    render(){
         const { closeModal2 } = this.props
        return( 
        <div className={styles.wrapperRed} onClick={closeModal2} >
                <div className={styles.modal} onClick={e=>{console.log(e.target);
                 e.preventDefault();
                 e.stopPropagation()}}>
                <div className={styles.header}>
                    <h1 className={styles.title}>This is SECOND MODAL!!! Do you want to delite this file?</h1>
                    <button className={styles.button} onClick={closeModal2} >{String.fromCharCode(215)}</button>
                </div>
                <div className={styles.content} > Once you delete this file, it won't be pissible tu undo this action. <br/> Are you sure you watnt to delete it?  </div>
                <div className={styles.buttons}>
                    <button className={styles.buttonOk}>Ok</button>
                    <Button closeModal= {closeModal2}/>
                    </div>
                </div>
            </div>)
           
        }

    }

    export default ModalSecond;